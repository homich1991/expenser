package com.homich.expenser

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import org.springframework.web.servlet.config.annotation.CorsRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer
import javax.annotation.PostConstruct


@SpringBootApplication
class ExpenserApplication {

    @Autowired
    private val objectMapper: ObjectMapper? = null

    @Bean
    fun corsConfigurer(): WebMvcConfigurer {
        return object : WebMvcConfigurer {
            override fun addCorsMappings(registry: CorsRegistry) {
                registry.addMapping("/**").allowedOrigins("http://localhost:3000")
            }
        }
    }

    @PostConstruct
    fun setUp() {
        objectMapper?.registerModule(JavaTimeModule())
    }
}

fun main(args: Array<String>) {
    runApplication<ExpenserApplication>(*args)
}
