package com.homich.expenser.util

import com.homich.expenser.entity.Category
import com.homich.expenser.entity.Expense
import com.homich.expenser.repository.CategoryRepository
import com.homich.expenser.repository.ExpenseRepository
import org.springframework.boot.CommandLineRunner
import org.springframework.stereotype.Component
import java.time.LocalDate

/**
 * TODO Write JavaDoc please.
 *
 * @author Roman_Meerson
 */
@Component
class DataFiller(val expenseRepository: ExpenseRepository,
                 val categoryRepository: CategoryRepository) : CommandLineRunner {

    override fun run(vararg args: String?) {
        fillCategory()
        fillExpense()
    }

    private fun fillCategory() {
        categoryRepository.saveAll(listOf(
                Category("Food"),
                Category("Auto"),
                Category("Tech")
        ))
    }

    private fun fillExpense() {
        expenseRepository.saveAll(listOf(
                Expense("Макдональдс", 300, LocalDate.now(), "Food"),
                Expense("Макдональдс", 400, LocalDate.now().minusDays(1), "Food"),
                Expense("Макдональдс", 500, LocalDate.now().minusDays(2), "Food"),
                Expense("Макдональдс", 250, LocalDate.now().minusDays(3), "Food"),
                Expense("Заправка", 2100, LocalDate.now(), "Auto"),
                Expense("Заправка", 2000, LocalDate.now().minusDays(7), "Auto"),
                Expense("Заправка", 1900, LocalDate.now().minusDays(14), "Auto"),
                Expense("Перчини", 666, LocalDate.now(), "Food"),
                Expense("Важная птица", 570, LocalDate.now().minusDays(4), "Food"),
                Expense("Важная птица", 400, LocalDate.now().minusDays(1), "Food"),
                Expense("Xiaomi Yi", 4500, LocalDate.now().minusDays(15), "Tech")
        ))
    }
}