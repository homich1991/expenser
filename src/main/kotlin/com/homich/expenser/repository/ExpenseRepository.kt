package com.homich.expenser.repository

import com.homich.expenser.entity.Expense
import org.springframework.data.jpa.repository.JpaRepository

/**
 * TODO Write JavaDoc please.
 *
 * @author Roman_Meerson
 */
interface ExpenseRepository : JpaRepository<Expense, Long>