package com.homich.expenser.repository

import com.homich.expenser.entity.Category
import org.springframework.data.jpa.repository.JpaRepository

/**
 * TODO Write JavaDoc please.
 *
 * @author Roman_Meerson
 */
interface CategoryRepository : JpaRepository<Category, String>