package com.homich.expenser.controller

import com.homich.expenser.annotation.ApiPageable
import com.homich.expenser.entity.Category
import com.homich.expenser.repository.CategoryRepository
import org.springframework.data.domain.Pageable
import org.springframework.web.bind.annotation.*
import springfox.documentation.annotations.ApiIgnore

/**
 * TODO Write JavaDoc please.
 *
 * @author Roman_Meerson
 */
@RestController
@RequestMapping("/api/category")
class CategoryController(val categoryRepository: CategoryRepository) {

    @GetMapping("/{id}")
    fun get(@PathVariable id: String) = categoryRepository.getOne(id)

    @PostMapping
    fun save(@RequestBody category: Category) = categoryRepository.save(category)

    @ApiPageable
    fun page(@ApiIgnore pageable: Pageable) = categoryRepository.findAll(pageable)
}