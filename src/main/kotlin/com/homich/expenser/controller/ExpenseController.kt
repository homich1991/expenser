package com.homich.expenser.controller

import com.homich.expenser.annotation.ApiPageable
import com.homich.expenser.entity.Expense
import com.homich.expenser.repository.ExpenseRepository
import org.springframework.data.domain.Pageable
import org.springframework.web.bind.annotation.*
import springfox.documentation.annotations.ApiIgnore

/**
 * TODO Write JavaDoc please.
 *
 * @author Roman_Meerson
 */
@RestController
@RequestMapping("/api/expense")
class ExpenseController(var expenseRepository: ExpenseRepository) {

    @GetMapping("/{id}")
    fun get(@PathVariable id: Long) = expenseRepository.getOne(id)

    @PostMapping()
    fun save(@RequestBody expense: Expense) = expenseRepository.save(expense)

    @GetMapping
    @ApiPageable
    fun page(@ApiIgnore pageable: Pageable) = expenseRepository.findAll(pageable)
}

