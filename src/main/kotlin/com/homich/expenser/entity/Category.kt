package com.homich.expenser.entity

import javax.persistence.Entity
import javax.persistence.Id

/**
 * TODO Write JavaDoc please.
 *
 * @author Roman_Meerson
 */
@Entity
class Category(@Id
               var name: String)