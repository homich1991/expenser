package com.homich.expenser.entity

import java.math.BigDecimal
import java.time.LocalDate
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

/**
 * TODO Write JavaDoc please.
 *
 * @author Roman_Meerson
 */
@Entity
class Expense(
        val name: String,
        val cost: BigDecimal,
        val date: LocalDate,
        val category: String) {

    constructor(name: String, cost: Long, date: LocalDate, category: String) : this(name, BigDecimal.valueOf(cost), date, category)

    @Id
    @GeneratedValue
    var id: Long? = null

}